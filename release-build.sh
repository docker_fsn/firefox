#!/bin/bash

set -eu

tagName=${1:-latest}
docker build -t fnascimento/firefox:$tagName . &&\
docker login &&\
docker push fnascimento/firefox:$tagName
