#!/bin/sh

set -eu

imageName=${1:-firefox}

docker container run \
	-it \
	--rm \
	--net=host \
	--env="DISPLAY" \
	--volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
	--volume=/tmp/firefox_extensions:/firefox_extensions \
	$imageName
	
