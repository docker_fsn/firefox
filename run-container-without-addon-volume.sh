#!/bin/sh

set -eu

imageName=${1:-fnascimento/firefox}

docker container run \
	-it \
	--rm \
	--net=host \
	--env="DISPLAY" \
	--volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
	$imageName
	
