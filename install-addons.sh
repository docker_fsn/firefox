#!/bin/bash

set -eu

cd /firefox_extensions

for extensionFile in *.xpi
do
	if [[ -f "$extensionFile" ]]; then
		UID_ADDON=$(unzip -p $extensionFile install.rdf | grep "<em:id>" | head -n 1 | sed 's/^.*>\(.*\)<.*$/\1/g')
		unzip $extensionFile -d /root/firefox/browser/extensions/$UID_ADDON
	fi
done
