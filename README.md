Mozilla Firefox Dockerfile
==========================
Executando a Imagem
--------------------
```
docker container run \
    -it \
    --rm \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    firefox
```
Executando a Imagem com Add-on
--------------------------------
Para instalar um add-on, faça o download do respectivo arquivo xpi numa pasta qualquer, e monte-a no volume /firefox_extensions. Os arquivos xpi serão instalados silenciosamente antes da execução do Mozilla Firefox.
```
docker container run \
    -it \
    --rm \
    --net=host \
    --env="DISPLAY" \
    --volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
    --volume=/tmp/firefox_extensions:/firefox_extensions \
    firefox
```