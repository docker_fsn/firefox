#!/bin/sh

set -eu

imageName=${1:-firefox}

if [[ $(docker images $imageName | grep $imageName) != "" ]]; then
	docker image rm $imageName
fi
docker image build -t $imageName .
docker container run \
	-it \
	--rm \
	--net=host \
	--env="DISPLAY" \
	--volume="$HOME/.Xauthority:/root/.Xauthority:rw" \
	--volume=/tmp/firefox_extensions:/firefox_extensions \
	$imageName
